# GCN for electrical grids

The model should help identify the lines of an electrical grid that are of interest when it comes to line switching to reduce the amount of redespatching.
This repository only contains the necessary files for training and testing the GCN model. This model is used for my OR internship. The full project is private, so I won't be able to share it. The model is still a work in progress. One possible improvement is to use a DQN together with the GCN, but this might not be feasable with the time constraints.
